
export const authStore = {
  isLoggedIn: () => {
    const token = localStorage.getItem('authToken');
    if(token) {
      return true;
    }
    return false;
  },
  // tslint:disable-next-line:object-literal-sort-keys
  hasToken: () => {
    const token = localStorage.getItem('authToken');
    if(token) {
      return true;
    }
    return false;
  },
  setToken: (token: string) => {
    localStorage.setItem('authToken', token);
  },
  logout: () => {
    localStorage.removeItem('authToken');
    window.location.href = '/login';
  }
}
