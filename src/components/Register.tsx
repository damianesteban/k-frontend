// tslint:disable:interface-name
 // tslint:disable:no-console
import gql from 'graphql-tag';
// import { HomeLink } from './Navbar';
import * as React from 'react';
import { Mutation } from 'react-apollo';
import {
  Redirect
} from 'react-router-dom'
// import { Redirect } from 'react-router';
import { Button, Form, Grid, Header } from 'semantic-ui-react'
import { authStore } from '../service/utils';

// Mutation
const signupUser = gql`
  mutation signUp($email: String!, $password: String!) {
    signUp(email: $email, password: $password) {
      token
    }
  }
`

// State
// tslint:disable-next-line:interface-name
interface RegisterState {
  email: string;
  password: string;
  hasToken: boolean;
}

// Register class
class Register extends React.Component<{}, RegisterState> {

  public state = {
    email: '',
    password: '',
    // tslint:disable-next-line:object-literal-sort-keys
    hasToken: authStore.hasToken(),
  }

  public onEmailUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    // tslint:disable-next-line:no-console
    console.log('Updating email...');
    console.log(event.currentTarget.value)
    this.setState({ email: event.currentTarget.value });
  }

  public onPasswordUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log('Updating password...');
    console.log(event.currentTarget.value);
    this.setState({ password: event.currentTarget.value });
  }

  public render() {

    if (this.state.hasToken === true) {
      return <Redirect to='/home' />
    }
    
    const { email, password } = this.state;
    return (
      <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          Sign Up
        </Header>
      <Form>
      <Form.Field>
        <label>Email</label>
        <input 
          placeholder='user@user.com' 
          onChange={this.onEmailUpdate}
        />
      </Form.Field>
      <Form.Field>
        <label>Password</label>
        <input 
          placeholder='password' 
          onChange={this.onPasswordUpdate}
        />
      </Form.Field>
      <Mutation
        mutation={signupUser}
        variables={{ email, password }}
        // tslint:disable-next-line:jsx-no-lambda
        onCompleted={ (data: any) => {
           console.log(data) 
           authStore.setToken(data.signUp.token);
           this.setState({ email: '', password: ''});
          }}
      >
        {(mutation: any) => (
          <Button
            type='submit'
            onClick={mutation}
          >
          Submit
         </Button>
        )}
      </Mutation>
    </Form>
    </Grid.Column>
    </Grid>
    )
  }
}

export { Register }
