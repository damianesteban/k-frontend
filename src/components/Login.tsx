// tslint:disable:interface-name
// tslint:disable:no-console
import gql from 'graphql-tag';
// import { HomeLink } from './Navbar';
import * as React from 'react';
import { Mutation } from 'react-apollo';
import {
   Redirect
} from 'react-router-dom'
//  import { Redirect } from 'react-router';
import { Button, Form, Grid, Header } from 'semantic-ui-react'
import { authStore } from '../service/utils';
 // Mutation
 const loginUser = gql`
   mutation signIn($email: String!, $password: String!) {
     signIn(email: $email, password: $password) {
       token
     }
   }
 `
 
 // State
 // tslint:disable-next-line:interface-name
 interface LoginState {
   email: string;
   password: string;
   toSignup: boolean;
 }

 // Login class
 class Login extends React.Component<{}, LoginState> {
 
   public state = {
     email: '',
     password: '',
     toSignup: false
   }
 
   public onEmailUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
     // tslint:disable-next-line:no-console
     console.log('Updating email...');
     console.log(event.currentTarget.value)
     this.setState({ email: event.currentTarget.value });
   }
 
   public onPasswordUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
     console.log('Updating password...');
     console.log(event.currentTarget.value);
     this.setState({ password: event.currentTarget.value });
   }

   public onClickSignup = (event: React.MouseEvent<HTMLButtonElement>) => {
    this.setState(() => ({
      toSignup: true
    }))
  }
 
   public render() {

    if (this.state.toSignup === true) {
      return <Redirect to='/signup' />
    }

     const { email, password } = this.state;
     return (
      <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          Log In
        </Header>
       <Form>
       <Form.Field>
         <label>Email</label>
         <input 
           placeholder='user@user.com' 
           onChange={this.onEmailUpdate}
         />
       </Form.Field>
       <Form.Field>
         <label>Password</label>
         <input 
           placeholder='password' 
           onChange={this.onPasswordUpdate}
         />
       </Form.Field>
       <Mutation
         mutation={loginUser}
         variables={{ email, password }}
         // tslint:disable-next-line:jsx-no-lambda
         onCompleted={ (data: any) => {
            console.log(data) 
            authStore.setToken(data.signIn.token);
           }}
       >
         {(mutation: any) => (
           <Button
             type='submit'
             onClick={mutation}
           >
           Submit
          </Button>
         )}
       </Mutation>
       <Button
        type='submit'
        // tslint:disable-next-line:jsx-no-lambda
        onClick={this.onClickSignup}
       >
           Signup
          </Button>
     </Form>
     </Grid.Column>
    </Grid>
     )
   }
 }
 
 export { Login }