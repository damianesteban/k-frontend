// tslint:disable:no-console
import gql from 'graphql-tag';
import * as React from 'react';
import { Mutation } from 'react-apollo';
import { Button, Form, Grid, Header, Icon } from 'semantic-ui-react';

const PROJECT_MUTATION = gql`
  mutation ProjectMutation($title: String!, $imageUrlString: String!, $userId: Int!) {
    createProject(title: $title, imageUrlString: $imageUrlString, userId: $userId) {
      id
      title
      imageUrlString
    }
  }
`

// tslint:disable-next-line:interface-name
interface CreateProjectState {
  title: string;
  imageUrlString: string;
  userId: number;
}

class NewProjectForm extends React.Component<{}, CreateProjectState> {

  public state = {
    title: '',
    // tslint:disable-next-line:object-literal-sort-keys
    imageUrlString: '',
    userId: 0,
  }

  public onTitleUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log('Updating title...');
    console.log(event.currentTarget.value);
    this.setState({ title: event.currentTarget.value });
  }

  public onImageUrlUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log('Updating title...');
    console.log(event.currentTarget.value);
    this.setState({ imageUrlString: event.currentTarget.value });
  }

  public render() {
    const { title, imageUrlString, userId } = this.state;
    return (
      <div>
      <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 700 }}>
      <Header as='h4' icon={true} textAlign='center'>
        <Icon name='archive' circular={true} />
        <Header.Content>Project</Header.Content>
      </Header>
      <Form>
        <Form.Field>
          <label>Title</label>
          <input 
          placeholder='Title'
          onChange={this.onTitleUpdate}
          />
        </Form.Field>
        <Form.Field>
          <label>Image URL</label>
          <input 
          placeholder='Image URL' 
          onChange={this.onImageUrlUpdate}
          />
        </Form.Field>
        <Mutation
          mutation={PROJECT_MUTATION}
          variables={{ title, imageUrlString, userId }}
          // tslint:disable-next-line:jsx-no-lambda
          onCompleted={ (data: any) => {
            // tslint:disable-next-line:no-console
            console.log(data);
          }}
          >
          {(mutation: any) => (
            <Button 
              type='submit'
              onClick={mutation}
            >
            Submit
          </Button>
          )}
        
      </Mutation>
    </Form>
    </Grid.Column>
    </Grid>
  </div>
  )
  }

      }

export { NewProjectForm };
