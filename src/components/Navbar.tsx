import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from 'react-apollo';

import * as React from 'react';
import { Menu } from 'semantic-ui-react';
import { Login } from './Login';
import { ProjectsList } from './ProjectsList';
import { Register } from './Register';
import { Home } from './TempHome';

// TODO: - Move to another file, restructure this whole mess
export const BaseLink = (props: any) => <Link to="/" { ...props} style={{ textDecoration: 'none'}}/>
export const HomeLink = (props: any) => <Link to="/" { ...props} style={{ textDecoration: 'none'}}/>
export const SignupLink = (props: any) => <Link to="/signup" {...props} style={{ textDecoration: 'none' }}/>

export const AppBar = () => 
  <Menu inverted={true} stackable={true}>
    <Menu.Item>
      Projects App
    </Menu.Item>
  </Menu>

const client = new ApolloClient({
	link: new HttpLink({ uri: 'http://35.232.45.16/api/graphql' }),
	// tslint:disable-next-line:object-literal-sort-keys
	cache: new InMemoryCache(),
	connectToDevTools: true
});

// TODO: = Same as above, restructure
const App = (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <div>
          <AppBar />
          <br />
          <Switch>
            <Route exact={true} path ="/" component={Login} />
            <Route path="/signup" component={Register} />
            <Route path="/home" component={Home} />
            <Route path="/projects" component={ProjectsList} />
          </Switch>
        </div>
      </BrowserRouter>
    </ApolloProvider>
)

export { App };