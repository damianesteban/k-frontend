import * as React from 'react';
import { Divider, Header } from 'semantic-ui-react'
import { NewProjectForm } from './NewProjectForm';

const Home = () => (
  <div>
  <Header as='h2' textAlign="center">
    Home
    <Header.Subheader>Add a new project below.</Header.Subheader>
  </Header>
  <Divider />
  <NewProjectForm />
  </div>
)

export { Home }

