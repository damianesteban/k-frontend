import * as React from 'react';
import { Card, Image } from 'semantic-ui-react';

// tslint:disable:interface-name
export interface Project {
  id: string;
  title: string;
  imageUrlString: string;
}
interface ProjectCardProps {
  project: Project;
}

const ProjectCard = (props: ProjectCardProps) => (
  <Card>
    <Image src={props.project.imageUrlString} />
    <Card.Content>
      <Card.Header>{props.project.title}</Card.Header>
      <Card.Meta>
        <span className='date'>Joined in 2015</span>
      </Card.Meta>
      <Card.Description>Matthew is a musician living in Nashville.</Card.Description>
    </Card.Content>
  </Card>
)

export { ProjectCard };
