// tslint:disable:curly
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';

import { Container, Grid, Header } from 'semantic-ui-react';

import * as React from 'react'
import { ProjectCard } from './ProjectCard';

const PROJECTS_QUERY = gql`
  {
    projects {
      id
      title
      imageUrlString
    }
  }
`;

export const ProjectsList = () => (
  <Query query={PROJECTS_QUERY}>
  { ({ loading, error, data }) => {
      
      // tslint:disable-next-line:curly
      if (loading) return 'Loading....';
      if (error) return `Error! ${error.message}`;
      
      return (
        <Container>
          <Header as='h2' inverted={true} textAlign='center'>
            Relaxed 4x8x4
          </Header>
          <Grid>
            <Grid.Row columns={3} divided={true}>
              <Grid.Column>
                {data.projects.map((project: any) => (
                  <ProjectCard project={project} />
                ))}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      )
    }}
</Query>
);
