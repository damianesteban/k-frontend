// import { InMemoryCache } from 'apollo-cache-inmemory'
// import { ApolloClient } from 'apollo-client'
// import { createHttpLink } from 'apollo-link-http'
// import { ApolloProvider } from 'react-apollo';
import * as ReactDOM from 'react-dom';
import { App } from './components/Navbar';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

// const httpLink = createHttpLink({
//   uri: 'http://35.232.45.16/api/graphql'
// });

// const client = new ApolloClient({
//   link: httpLink,
//   // tslint:disable-next-line:object-literal-sort-keys
//   cache: new InMemoryCache()
// })


ReactDOM.render(
  // <ApolloProvider client={client}>
    App,
  // </ApolloProvider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
